'use strict';
module.exports = (sequelize, DataTypes) => {
  var like = sequelize.define('like', {
    messageId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Message',
        key: 'id'
      }
    },
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'User',
        key: 'id'
      }
    },
    isLike: DataTypes.INTEGER
  }, {});
  like.associate = function(models) {
    // associations can be defined here

    models.User.belongsToMany(models.Message, {
      through: models.like,
      foreignKey: 'userId',
      otherKey: 'messageId',
    });

    models.Message.belongsToMany(models.User, {
      through: models.like,
      foreignKey: 'messageId',
      otherKey: 'userId',
    });

    models.like.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user',
    });

    models.like.belongsTo(models.Message, {
      foreignKey: 'messageId',
      as: 'message',
    });
  };
  return like;
};