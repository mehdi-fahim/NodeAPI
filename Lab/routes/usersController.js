//Imports
let bcrypt   = require('bcrypt');
let jwtUtils = require('../utils/jwt.utils');
let models   = require('../models');
let asyncLib    = require('async');

// Constants
const EMAIL_REGEX     = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX  = /^(?=.*\d).{4,8}$/;

//Routes
module.exports = {
    register: function(req, res){
        //Params
        let email   = req.body.email;
        let username   = req.body.username;
        let password   = req.body.password;
        let bio   = req.body.bio;

        if (email == null || username == null || password == null){
            return res.status(400).json({'error': 'missing parameters'})
        }

        if (username.length >= 13 || username.length <= 4) {
            return res.status(400).json({ 'error': 'wrong username (must be length 5 - 12)' });
        }
      
        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({ 'error': 'email is not valid' });
        }
    
        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({ 'error': 'password invalid (must length 4 - 8 and include 1 number at least)' });
        }

        asyncLib.waterfall([
            function(done) {
                done(null, 'var1');
            },
            function(var1, done) {

            }
        ], function(err) {
            if(!err) {
                return res.status(200).json({ 'msg': 'ok' })
            } else {
                return res.status(404).json({ 'msg': 'error' })
            }
        });

        asyncLib.waterfall([
            function(done) {
              models.user.findOne({
                attributes: ['email'],
                where: { email: email }
              })
              .then(function(userFound) {
                done(null, userFound);
              })
              .catch(function(err) {
                return res.status(500).json({ 'error': 'unable to verify user' });
              });
            },
            function(userFound, done) {
              if (!userFound) {
                bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
                  done(null, userFound, bcryptedPassword);
                });
              } else {
                return res.status(409).json({ 'error': 'user already exist' });
              }
            },
            function(userFound, bcryptedPassword, done) {
              var newUser = models.user.create({
                email: email,
                username: username,
                password: bcryptedPassword,
                bio: bio,
                isAdmin: 0
              })
              .then(function(newUser) {
                done(newUser);
              })
              .catch(function(err) {
                return res.status(500).json({ 'error': 'cannot add user' });
              });
            }
          ], function(newUser) {
            if (newUser) {
              return res.status(201).json({
                'userId': newUser.id
              });
            } else {
              return res.status(500).json({ 'error': 'cannot add user' });
            }
          });
        },
    login: function(req, res){
        //Params
        var email    = req.body.email;
        var password = req.body.password;

        if(email == null || password == null){
            return res.status(400).json({ 'error': 'missing parameters' });
        }

        // TODO verify mail & password regex 

        models.user.findOne({
            attributes: ['email'],
            where: {email: email }
        })
        .then(function(userFound) {
            if(userFound){
                bcrypt.compare(password, userFound.password, function(errBcrypt, resBcrypt) {
                    if(resBcrypt){
                        return res.status(200).json({
                            'userId': newUser.id,
                            'token': jwtUtils.generateTokenForUser(userFound)
                        });
                    } else {
                        return res.status(403).json({'error': 'invalid password'})
                    }
                })
            } else {
                return res.status(404).json({'error': 'user noy exist in DB'})
            }
        })
        .catch(function(err) {
            return res.status(500).json({'error': 'unable to verify user'})
        })
    },
    getUserProfile: function(req, res) {
        // Getting auth header
        var headerAuth  = req.headers['authorization'];
        var userId      = jwtUtils.getUserId(headerAuth);
    
        if (userId < 0)
          return res.status(400).json({ 'error': 'wrong token' });
    
        models.User.findOne({
          attributes: [ 'id', 'email', 'username', 'bio' ],
          where: { id: userId }
        }).then(function(user) {
          if (user) {
            res.status(201).json(user);
          } else {
            res.status(404).json({ 'error': 'user not found' });
          }
        }).catch(function(err) {
          res.status(500).json({ 'error': 'cannot fetch user' });
        });
    },
    updateUserProfile: function(req, res) {
        // Getting auth header
        var headerAuth  = req.headers['authorization'];
        var userId      = jwtUtils.getUserId(headerAuth);

        // Params
        var bio = req.body.bio;

        asyncLib.waterfall([
            function(done) {
            models.User.findOne({
                attributes: ['id', 'bio'],
                where: { id: userId }
            }).then(function (userFound) {
                done(null, userFound);
            })
            .catch(function(err) {
                return res.status(500).json({ 'error': 'unable to verify user' });
            });
            },
            function(userFound, done) {
            if(userFound) {
                userFound.update({
                bio: (bio ? bio : userFound.bio)
                }).then(function() {
                done(userFound);
                }).catch(function(err) {
                res.status(500).json({ 'error': 'cannot update user' });
                });
            } else {
                res.status(404).json({ 'error': 'user not found' });
            }
            },
        ], function(userFound) {
            if (userFound) {
            return res.status(201).json(userFound);
            } else {
            return res.status(500).json({ 'error': 'cannot update user profile' });
            }
        });
    }
}