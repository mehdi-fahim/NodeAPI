//Imports
let express         = require('express');
let usersCtrl       = require('./routes/usersController');
let messagesCtrl    = require('./routes/messagesController');
let likesCtrl       = require('./routes/likesController');

//Router
exports.router = (function () {
    let apiRouter = express.Router();

    //Users Routes
    apiRouter.route('/register').post(usersCtrl.register);
    apiRouter.route('/login').post(usersCtrl.login);
    apiRouter.route('/profil').get(usersCtrl.getUserProfile);
    apiRouter.route('/profil').put(usersCtrl.updateUserProfile);

    //Messages Routes
    apiRouter.route('/messages/new').post(messagesCtrl.createMessage);
    apiRouter.route('/messages').post(messagesCtrl.listMessages);

    // Likes
  apiRouter.route('/messages/:messageId/vote/like').post(likesCtrl.likePost);
  apiRouter.route('/messages/:messageId/vote/dislike').post(likesCtrl.dislikePost);

    return apiRouter;
})();