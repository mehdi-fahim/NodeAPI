//Imports
let express     = require('express');
let bodyParser  = require('body-parser');
let apiRouter   = require('./apiRouter').router;

//Init Server
let server = express();

//Body Parser Config
server.use(bodyParser.urlencoded({ extend: true }));
server.use(bodyParser.json());

//Config Routes
server.get('/', function(req, res){
    res.setHeader('Content-type', 'text/html');
    res.status(200).send('<h1>Bienvenue sur le server</h1>')
});

server.use('/api/', apiRouter);

//Start Server
server.listen(3000, function(){
   console.log('Server Start');
});