FROM node:10.4.1-stretch

RUN apt update

RUN apt install -y \
    git \
    curl \
    npm

RUN docker-php-ext-install -j$(nproc) \
    intl \
    gd \
    pdo \
    curl \
    json \
    gmp \
    mbstring \
    xml \
    zip

RUN docker-php-ext-configure \
    pdo_mysql

RUN docker-php-ext-install -j$(nproc) \
    pdo_mysql