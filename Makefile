build:
	docker-compose build
up:
	docker-compose up -d
stop:
	docker-compose stop
rm:
	docker-compose rm -f
kill:
	docker-compose kill

logs:
	docker-compose logs

tail:
	docker-compose logs -f 


destroy:
	make stop
	make kill
	make rm

reboot:
	make stop
	make kill
	make up
	docker-compose exec api_node npm update

install:
	make build
	make up

reinstall:
	make destroy
	make install

#===========================#
#		 	API				#
#===========================#

bash.api:
	docker-compose exec api_node bash

exec.api:
	docker-compose exec api_node

tail.api:
	docker-compose logs -f api_node

logs.api:
	docker-compose logs api_node

#===========================#
#		 	DB				#
#===========================#

bash.db:
	docker-compose exec database bash

exec.db:
	docker-compose exec database

tail.db:
	docker-compose logs -f database

logs.db:
	docker-compose logs database